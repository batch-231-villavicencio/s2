// without the use objects, our students from before would be organized as follows if we are to record additional information about them
// spaghetti code - code is not organized enough that it becomes hard to work with
// encapsulation - organize related information (properties) and behavior (methods) to belong to a single entity
//create student 

// Encapsulate the following information into 4 student objects using object literals

// let studentOneName = 'Tony';
// let studentOneEmail = 'starksindustries@mail.com';
// let studentOneGrades = [89, 84, 78, 88];

// //create student two
// let studentTwoName = 'Peter';
// let studentTwoEmail = 'spideyman@mail.com';
// let studentTwoGrades = [78, 82, 79, 85];

// //create student three
// let studentThreeName = 'Wanda';
// let studentThreeEmail = 'scarlettMaximoff@mail.com';
// let studentThreeGrades = [87, 89, 91, 93];

// //create student four
// let studentFourName = 'Steve';
// let studentFourEmail = 'captainRogers@mail.com';
// let studentFourGrades = [91, 89, 92, 93];

// //actions that students may perform will be lumped together
// function login(student){
//     console.log(`${this.email} has logged in`);
// }

// function logout(student){
//     console.log(`${student.email} has logged out`);
// }

// function listGrades(student){
//     console.log(student)
//     student.grades.forEach(grade => {
//         console.log(grade);
//     })
// }


// function passGrade(){
//     let sum = 0;
//     this.grades.forEach(grade => sum = sum+grade)
//     return (sum/4 >= 85)
// }

let studentOne = {
    name: "Tony",
    email: "starksindustries@mail.com",
    grades: [89, 84, 78, 88],
    login(){
        console.log(`${this.email} has logged in`);
    },
    logout(){
        console.log(`${this.email} has logged out`)
    },
    listGrades(){
        this.grades.forEach(grade => {
            console.log(grade)
        })
    },
    computeAve(){
        let sum = 0
        this.grades.forEach(grade => sum = sum+grade)
        return sum/4
    },
    willPass(){
        return this.computeAve() >= 85 ? true : false
    },
    willPassWithHonors(){
        return (this.willPass() && this.computeAve() >= 90) ? true : false
    }
}


/*
    1. Spaghetti Code
    2. {}
    3. Encapsulation
    4. studentOne.enroll()
    5. True
    6. global window object
    7. True
    8. True
    9. True
    10. True
*/

let studentTwo = {
    name: "Peter",
    email: "spideyman@mail.com",
    grades: [78, 82, 79, 85],
    login(){
        console.log(`${this.email} has logged in`);
    },
    logout(){
        console.log(`${this.email} has logged out`)
    },
    listGrades(){
        this.grades.forEach(grade => {
            console.log(grade)
        })
    },
    computeAve(){
        let sum = 0
        this.grades.forEach(grade => sum = sum+grade)
        return sum/4
    },
    willPass(){
        return this.computeAve() >= 85 ? true : false
    },
    willPassWithHonors(){
        return (this.willPass() && this.computeAve() >= 90) ? true : false
    }
}

let studentThree = {
    name: "Wanda",
    email: "scarlettMaximoff@mail.com",
    grades: [87, 89, 91, 93],
    login(){
        console.log(`${this.email} has logged in`);
    },
    logout(){
        console.log(`${this.email} has logged out`)
    },
    listGrades(){
        this.grades.forEach(grade => {
            console.log(grade)
        })
    },
    computeAve(){
        let sum = 0
        this.grades.forEach(grade => sum = sum+grade)
        return sum/4
    },
    willPass(){
        return this.computeAve() >= 85 ? true : false
    },
    willPassWithHonors(){
        return (this.willPass() && this.computeAve() >= 90) ? true : false
    }
}

let studentFour = {
    name: "Steve",
    email: "captainRogers@mail.com",
    grades: [91, 89, 92, 93],
    login(){
        console.log(`${this.email} has logged in`);
    },
    logout(){
        console.log(`${this.email} has logged out`)
    },
    listGrades(){
        this.grades.forEach(grade => {
            console.log(grade)
        })
    },
    computeAve(){
        let sum = 0
        this.grades.forEach(grade => sum = sum+grade)
        return sum/4
    },
    willPass(){
        return this.computeAve() >= 85 ? true : false
    },
    willPassWithHonors(){
        return (this.willPass() && this.computeAve() >= 90) ? true : false
    }
}

const classOf1A = {
    students: [studentOne, studentTwo, studentThree, studentFour],

    countHonorStudents(){
        let result = 0
        this.students.forEach(student => {
            if (student.willPassWithHonors()){
                result++;
            }
        })
        return result;
    },
    honorsPercentage(){
        percentage = (this.countHonorStudents() / this.students.length) * 100
        return percentage
    },
    retrieveHonorStudentInfo(){
        let newArr = this.students.map(function(student){
            if (student.willPassWithHonors()){
                return ({
                    email: student.email,
                    aveGrade: student.computeAve()
                })
            }
        })
        newArr = newArr.filter(function(elem){
            return elem !== undefined;
        })
        return newArr
    },
    sortHonorStudentsByGradeDesc(){
        let newArr = this.retrieveHonorStudentInfo().sort((a,b) => b.aveGrade - a.aveGrade)
        return newArr
    }

}